import { createRouter, createWebHashHistory } from 'vue-router'
import network from './maps/network'
import routing from './maps/routing'

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: '/',
      redirect: '/network/index'
    },
    {
      path: '/',
      component: () => import('@/components/system/container.vue'),
      children: [
        ...network,
        ...routing,
      ]
    }
  ]
})

export default router