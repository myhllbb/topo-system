export default [
  {
    path: 'network/linkChart',
    name: 'networkLinkChart',
    component: () => import('@/pages/network/linkChart')
  },
  {
    path: 'network/topologyChart',
    name: 'networkTopologyChart',
    component: () => import('@/pages/network/topologyChart')
  },{
    path: 'network/index',
    name: 'networkManage',
    component: () => import('@/pages/network/index')
  },
  {
    path:'network/history_para',
    name: 'newworkpara',
    component:() => import('@/pages/network/history_para')
  },
  {
    path:'network/singleTopo',
    name: 'singleTopo',
    component:() => import('@/pages/network/singleTopo')
  },
  {
    path:'network/businessTotal',
    name: 'businessTotal',
    component:() => import('@/pages/network/businessTotal')
  },

]